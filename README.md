# Case study: Predicting penguin species using the k-Nearest Neighbors algorithm

A working example of using online k-nearest neighbors to classify
Palmer Penguins by species.

## Install the Palmer Penguins data and loading code

```
git clone https://gitlab.com/brohrer/study-knn-penguins.git
```

## Install Cottonwood if you haven't already

```
git clone https://gitlab.com/brohrer/cottonwood.git
python3 -m pip install -e cottonwood
```
Make sure you're using the right version.
```
cd cottonwood
git checkout v31
```

## Run the demo

```
cd study-knn-penguins
python3 classify_penguins.py
```

When you run it, this code will load the Palmer Penguins data, and use the Cottonwood online implementation of k-Nearest Neighbors to classify each penguin into one of three species. When it's done (in less than a second, typically), you'll get a short report of the form

```
3 out of 327 penguins misclassified for an accuracy of 99.1 percent.
```

and a plot of the categorization history, with errors shown as spikes.

![penguin performance plot](penguin_miscategorizations.png)

## Learn more

The k-NN algorithm and the Palmer Penguins data set are covered in detail in [End to End Machine Learning Course 221](https://e2eml.school/221). Cottonwood and its uses are covered in detail in [its GitLab repository](https://e2eml.school/cottonwood) and [an End to End Machine Learning course sequence](https://e2eml.school/catalog). 
