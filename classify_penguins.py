"""
This code makes use of the Cottonwood machine learning framework.
Get it at https://e2eml.school/cottonwood
"""
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.structure import Structure
from cottonwood.knn import KNN
from penguins import Data, n_penguins


# Create the k-NN model in Cottonwood.
model = Structure()

# Initialize the blocks.
model.add(Data(), "data")
model.add(KNN(), "knn")

# Hook them up together.
model.connect("data", "knn")
model.connect("data", "knn", i_port_tail=1, i_port_head=1)

misses = []
for i_iter in range(int(n_penguins)):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

misses = np.array(misses)
accuracy = 1 - np.mean(misses)
print(f"{np.sum(misses)} out of " +
        f"{misses.size} penguins misclassified " +
        f"for an accuracy of {accuracy * 100:.03} percent.")

fig = plt.figure()
ax = fig.gca()
ax.plot(misses, color="#04253a")
ax.set_title(f"k-NN with Palmer Penguins, k = {model.blocks['knn'].k}")
ax.set_xlabel("Iteration")
ax.set_ylabel("Missed penguins")
ax.grid()
plt.savefig("penguin_miscategorizations.png", dpi=300)
plt.show()
