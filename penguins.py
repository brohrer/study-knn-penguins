"""
Use k-nearest neighbors to identify penguins.
The data is downloaded from
https://raw.githubusercontent.com/mcnakhaee/palmerpenguins/master/palmerpenguins/data/penguins.csv
and the data set is metculously documented here
https://github.com/allisonhorst/palmerpenguins/blob/master/README.md

Data citation:
   Horst AM, Hill AP, Gorman KB (2020). palmerpenguins: Palmer
   Archipelago (Antarctica) penguin data. R package version 0.1.0.
   https://allisonhorst.github.io/palmerpenguins/. doi:
   10.5281/zenodo.3960218.

License: CC0 Public Domain

Before the data is fed to forward_out() it is converted to numeric values.

Numeric code for sex:
male:0
female: 1

Numeric code for species labels
Adelie: 0
Chinstrap: 1
Gentoo: 2

Feature columns
0: bill length (mm)
1: bill depth (mm)
2: flipper length (m)
3: body mass(kg)
4: sex
"""
import os
import numpy as np

data_filename = os.path.join("data", "penguins.csv")
# The number of penguins with no NA values
n_penguins = 333

class Data:
    def __init__(self, repeat=False):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

        with open(data_filename, "rt") as f:
            data_lines = f.readlines()
            # The first row is full of column labels.
            # column_labels = data_lines[0].split(",")

        self.n_features = 5
        all_features = []
        all_labels = []

        # Describe how to convert sex and species text fields to numbers.
        sex_conversion = {"male": 0, "female": 1}
        label_conversion = {
            "Adelie": 0,
            "Chinstrap": 1,
            "Gentoo": 2,
        }

        # For each penguin, split the line up by commas, ignore
        # any residual whitespace on the ends, and pull out
        # the feature and label fields.
        # Start from row 1 so as to skip the column headings.
        # The try-except block catches the cases where the data is
        # missing and is replaced with and "NA". For now, we're choosing to
        # ignore all these data points.
        for i_penguin, line in enumerate(data_lines[1:]):
            line_data = line.split(",")
            try:
                penguin_features = np.zeros(self.n_features)
                penguin_features[:4] = [
                    float(x.rstrip()) for x in line_data[2:6]]
                # Convert sex and species to numbers.
                penguin_features[4] = sex_conversion[line_data[6].rstrip()]
                all_features.append(penguin_features)
                all_labels.append(label_conversion[line_data[0].rstrip()])
            except ValueError:
                # If any NA's are encountered in the numerical fields
                # just move along to the next penguin.
                pass
            except KeyError:
                # If any NA's are encountered in the sex conversion
                # just move along to the next penguin.
                pass

        self.features = np.array(all_features)
        self.labels = np.array(all_labels)

        # Put the data points in a random order.
        i_order = np.arange(self.labels.size)
        np.random.shuffle(i_order)
        self.features = self.features[i_order, :]
        self.labels = self.labels[i_order]
        self.i_penguin = 0
        self.repeat = repeat

    def __str__(self):
        str_parts = [
            "Penguin data set",
            f"penguins represented: {self.labels.size}",
            f"feature count: {self.features.shape[1]}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.i_penguin >= self.labels.size:
            if self.repeat:
                i_order = np.arange(self.labels.size)
                np.random.shuffle(i_order)
                self.features = self.features[i_order, :]
                self.labels = self.labels[i_order]
                self.i_penguin = 0
            else:
                raise StopIteration

        self.forward_out = (
            self.features[self.i_penguin, :], self.labels[self.i_penguin])
        self.i_penguin += 1
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out


if __name__ == "__main__":
    penguins = Data(repeat=False)
    print(penguins)
    for i in range(10):
        print(penguins.forward_pass(None))
